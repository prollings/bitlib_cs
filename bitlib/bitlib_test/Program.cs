﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

using bitlib;

namespace bitlib_test
{
    using bl = BitLib;

    class Program
    {
        static void Main(string[] args)
        {
            int MY_DEVICES = 1;
            string MY_PROBE_FILE = "";
            int MY_DEVICE = 0;
            int MY_CHANNEL = 0;
            int MY_MODE = bl.MODE_FAST;

            int MY_RATE = 1000000;
            int MY_SIZE = 40;

            if (bl.open(MY_PROBE_FILE, MY_DEVICES) == 0)
            {
                Console.WriteLine("Failed to find a device.");
                return;
            }

            if (bl.select(bl.SELECT_DEVICE, MY_DEVICE) != MY_DEVICE)
            {
                Console.WriteLine("Failed to select device.");
                return;
            }

            if (bl.select(bl.SELECT_CHANNEL, MY_CHANNEL) != MY_CHANNEL)
            {
                Console.WriteLine("Failed to select channel.");
            }

            /* 
             * Prepare to capture one channel
             */
            if (bl.mode(MY_MODE) != MY_MODE)
            {
                Console.WriteLine("Failed to select mode.");
                return;
            }

            bl.intro(bl.ZERO);
            bl.delay(bl.ZERO);
            bl.rate(MY_RATE);
            bl.size(MY_SIZE);
            bl.select(bl.SELECT_CHANNEL, MY_CHANNEL);
            bl.trigger(bl.ZERO, bl.TRIG_RISE);
            bl.select(bl.SELECT_SOURCE, bl.SOURCE_POD);
            bl.range(bl.count(bl.COUNT_RANGE));
            bl.enable(true);
            
            /*
             * Capture and acquire data
             */

            if (bl.trace(bl.TRACE_FORCED, false))
            {
                int n = MY_SIZE;
                double[] d = new double[MY_SIZE];
                bl.select(bl.SELECT_CHANNEL, MY_CHANNEL);
                if (bl.acquire(n, d) == n)
                {
                    Console.WriteLine("Acquired:");
                    for (int iii = 0; iii < n; iii++)
                    {
                        Console.Write(" ");
                        Console.Write(d[iii]);
                    }
                    Console.WriteLine();
                }
                bl.close();
                Console.ReadLine();
            }
        }
    }
}
