﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace bitlib
{
	public class BitLib
	{
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Acquire(int count, double[] arr);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern void   BL_Close();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Count(int type);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Coupling(int type);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern double BL_Delay(double time);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern bool   BL_Enable(bool flag);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Error();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern bool   BL_Halt();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern char[] BL_ID();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern bool   BL_Index(int index);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern void   BL_Initialize();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern double BL_Intro(double time);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern char[] BL_Log();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Mode(int mode);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern char[] BL_Name(char[] name);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern double BL_Offset(double offset);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Open(char[] pf, int count);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern double BL_Range(int range);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern bool   BL_Receive(char[] bc, int a, int b);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern double BL_Rate(double rate);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Select(int type, int id);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern void   BL_Send(char[] bc, int count);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_Size(int size);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern int    BL_State();
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern double BL_Time(double time);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern bool   BL_Trace(double timeout, bool async);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern bool   BL_Trigger(double level, int type);
		[DllImport("BitLib.dll", CallingConvention=CallingConvention.Cdecl)]
		private static extern char[] BL_Version(int type);

		public static int acquire(int count, double[] array)
		{
			return BL_Acquire(count, array);
		}

		public static void close()
		{
			BL_Close();
		}

		public static int count(int type)
		{
			return BL_Count(type);
		}

		public static int coupling(int type)
		{
			return BL_Coupling(type);
		}

		public static double delay(double time)
		{
			return BL_Delay(time);
		}

		public static bool enable(bool flag)
		{
			return BL_Enable(flag);
		}

		public static int error()
		{
			return BL_Error();
		}

		public static bool halt()
		{
			return BL_Halt();
		}

		public static string id()
		{
			string s = new string(BL_ID());
			return s;
		}

		public static bool index(int idx)
		{
			return BL_Index(idx);
		}

		public static void initialise()
		{
			BL_Initialize();
		}

		public static void initialize()
		{
			BL_Initialize();
		}

		public static double intro(double time)
		{
			return BL_Intro(time);
		}

		public static string log()
		{
			string l = new string(BL_Log());
			return l;
		}

		public static int mode(int mode)
		{
			return BL_Mode(mode);
		}

		public static string name(string name)
		{
            // DOESN'T WORK; WILL CRASH
            BL_Name(name.ToCharArray());
            return "";
		}

		public static double offset(double level)
		{
			return BL_Offset(level);
		}
	
		public static int open(string pf, int count)
		{
			return BL_Open(pf.ToCharArray(), count);
		}

		public static double range(int range)
		{
			return BL_Range(range);
		}

		public static bool receive(string bc, int a, int b)
		{
			return BL_Receive(bc.ToCharArray(), a, b);
		}

		public static double rate(double rate)
		{
			return BL_Rate(rate);
		}

		public static int select(int type, int id)
		{
			return BL_Select(type, id);
		}

		public static void send(string bc, int count)
		{
			BL_Send(bc.ToCharArray(), count);
		}

		public static int size(int size)
		{
			return BL_Size(size);
		}

		public static int state()
		{
			return BL_State();
		}

		public static double time(double time)
		{
			return BL_Time(time);
		}

		public static bool trace(double timeout, bool async)
		{
			return BL_Trace(timeout, async);
		}

		public static bool trigger(double level, int type)
		{
			return BL_Trigger(level, type);
		}

		public static string version(int type)
		{
			string s = new string(BL_Version(type));
			return s;
		}

		public static int MAX_RATE      = 0;
		public static int MAX_TIME      = 0;
		public static int MAX_SIZE      = 0;
		public static int TRACE_FORCED  = 0;
		public static int TRACE_FOREVER = -1;
		public static int ZERO          = 0;
		public static int ASK           = -1;

		public static int COUNT_DEVICE = 0;
        public static int COUNT_ANALOG = 1;
		public static int COUNT_LOGIC  = 2;
		public static int COUNT_RANGE  = 3;

		public static int SELECT_DEVICE  = 0;
		public static int SELECT_CHANNEL = 1;
		public static int SELECT_SOURCE  = 2;

		public static int STATE_IDLE   = 0;
		public static int STATE_ACTIVE = 1;
		public static int STATE_DONE   = 2;
		public static int STATE_ERROR  = 3;

		public static int SOURCE_POD = 0;
		public static int SOURCE_BNC = 1;
		public static int SOURCE_X10 = 2;
		public static int SOURCE_X20 = 3;
		public static int SOURCE_X50 = 4;
		public static int SOURCE_ALT = 5;
		public static int SOURCE_GND = 6;

		public static int COUPLING_DC = 0;
		public static int COUPLING_AC = 1;
		public static int COUPLING_RF = 2;

		public static int MODE_FAST   = 0;
		public static int MODE_DUAL   = 1;
		public static int MODE_MIXED  = 2;
		public static int MODE_LOGIC  = 3;
		public static int MODE_STREAM = 4;

		public static int TRIG_RISE = 0;
		public static int TRIG_FALL = 1;
		public static int TRIG_HIGH = 2;
		public static int TRIG_LOW  = 3;
		public static int TRIG_NONE = 4;

		public static int VERSION_DEVICE    = 0;
		public static int VERSION_LIBRARY   = 1;
		public static int VERSION_BINDING   = 2;
		public static int VERSION_PLATFORM  = 3;
		public static int VERSION_FRAMEWORK = 4;
		public static int VERSION_NETWORK   = 5;

		public static int SYNCHRONOUS = 0;
		public static int ASYNCHRONOUS = 1;
	}
}
